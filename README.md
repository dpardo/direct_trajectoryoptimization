# Direct Trajectory Optimization Package #

A C++ package for solving Trajectory Optimization problems using direct transcription and multiple shooting methods

This package provides a class to formulate a multi-phase continuous trajectory optimization problem including initial and terminal conditions,
path constraints and bounds in the state and control trajectories.

 This class automatically translate the continuous formulation into a discrete optimization problem and uses a (third-party) NLP solver to obtain
 a feasible and optimal solution.

 Main features :
 
 - Direct transcription using Hermite-Simpson and Trapezoidal interpolation
 - Direct Multiple Shooting
 - Interface with and Snopt
 - Multiple phase TO problem can be defined
 - Hybrid and Discontinuous Dynamics

### Contributors ###

This project is led by Diego Pardo (core developer) and contains knowledge and expertise
from the Agile and Dexterous Robotics Lab at ETH Zürich

Collaborators:
 - Michael Neunert
 - Harsoveet Singh
 - Lukas Möller
 - Alexander Winkler
 - Bob Hoffmann

### Contact ###

Diego Pardo depardo(at)ethz(dot)ch