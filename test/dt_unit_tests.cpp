/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * dt_unit_tests.cpp
 *
 *  Created on: Nov 19 2015
 *      Author: depardo
 */
#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <memory>
#include <iostream>

#include <method_interfaces/direct_transcription/direct_transcription.hpp>
#include <method_interfaces/multiple_shooting/multiple_shooting.hpp>

#include <optimization/constraints/ConstraintsBase.hpp>
#include <optimization/costs/CostFunctionBase.hpp>
#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/base/DerivativesNumDiffBase.hpp>

#include <optimization/costs/MinimumEnergyCostBolza.hpp>
#include <optimization/constraints/CylindricalObstaclePathConstraint.hpp>

#include <dynamical_systems/systems/acrobot/acrobotDimensions.hpp>
#include <dynamical_systems/systems/acrobot/acrobot_dynamics.hpp>
#include <dynamical_systems/systems/acrobot/acrobot_derivatives.hpp>

typedef acrobot::acrobotDimensions::state_vector_t state_vector_t;
typedef acrobot::acrobotDimensions::state_vector_array_t state_vector_array_t;
typedef acrobot::acrobotDimensions::control_vector_t control_vector_t;
typedef acrobot::acrobotDimensions::control_vector_array_t control_vector_array_t;
typedef DirectTrajectoryOptimization::DTODirectTranscription<acrobot::acrobotDimensions> test_interface_t;
//typedef DirectTrajectoryOptimization::DTOMultipleShooting<acrobot::acrobotDimensions> test_interface_t;

using namespace DirectTrajectoryOptimization;

int main(int argc, const char* argv[]) {

	std::shared_ptr<DynamicsBase<acrobot::acrobotDimensions> > dynamic1(
			new acrobotDynamics);
	std::shared_ptr<DerivativesBaseDS<acrobot::acrobotDimensions> > derivative1(
			new acrobotNumDiffDerivatives(dynamic1));
	std::shared_ptr<BaseClass::CostFunctionBase<acrobot::acrobotDimensions> > costFunction(
			new Costs::MinimumEnergyCostBolza<acrobot::acrobotDimensions>);
	std::shared_ptr<BaseClass::ConstraintsBase<acrobot::acrobotDimensions> > constraints1(
			new CylindricalObstaclePathConstraint<acrobot::acrobotDimensions>);
	std::shared_ptr<BaseClass::ConstraintsBase<acrobot::acrobotDimensions> > noconstraints1(
			new BaseClass::ConstraintsBase<acrobot::acrobotDimensions>);

	std::vector<std::shared_ptr<DynamicsBase<acrobot::acrobotDimensions> > > dynamics;
	dynamics.push_back(dynamic1);

	std::vector<std::shared_ptr<DerivativesBaseDS<acrobot::acrobotDimensions> > > derivatives;
	derivatives.push_back(derivative1);

	std::vector<std::shared_ptr<BaseClass::ConstraintsBase<acrobot::acrobotDimensions> > > constraints;
	constraints.push_back(constraints1);

	std::vector<std::shared_ptr<BaseClass::ConstraintsBase<acrobot::acrobotDimensions> > > noconstraints;
	noconstraints.push_back(noconstraints1);


	Eigen::VectorXd clb(constraints[0]->getNumConstraints());
	Eigen::VectorXd cub(constraints[0]->getNumConstraints());

	cub.setConstant(infy);
	clb.setConstant(-infy);

	constraints[0]->setConstraintsLowerBound(clb);
	constraints[0]->setConstraintsUpperBound(cub);

	Eigen::Vector2d duration;

	duration[0] = 2;
	duration[1] = 6;

	int number_of_nodes = 4;

	int dircol_method = 0;

	bool methodVerbose = true;

	//Validate no contraints
	{
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);
		my_dt.Initialize();
		std::cout << "end" << std::endl;
		getchar();
	}

	// Validate Durations
	std::cout << ">>>>>>>>>> validate_duration()" << std::endl;
	{
		duration[0] = 6;
		duration[1] = 0.1;
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		std::cout << "This should be 1 : " << my_dt.ValidateDuration()
				<< std::endl;
	}

	{
		duration[0] = 0.1;
		duration[1] = 6;
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		std::cout << "This should be 0 : " << my_dt.ValidateDuration()
				<< std::endl;

	}

	std::cout << ">>>>>>>>>> validate_states_actions_bounds()" << std::endl;
	{

		std::cout << "*** Default values ***" << std::endl;
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		std::cout << "This should be 0 : "
				<< my_dt.ValidateStatesActionsBounds() << std::endl;
	}

	{
		std::cout << "*** Setting wrong control bounds ***" << std::endl;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		control_vector_t ulb;
		ulb.setConstant(10);
		control_vector_t uub;
		uub.setConstant(-99);

		my_dt.SetControlBounds(ulb, uub);

		std::cout << "This should be 1 : "
				<< my_dt.ValidateStatesActionsBounds() << std::endl;
	}
	{
		std::cout << "*** Setting wrong state bounds ***" << std::endl;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		state_vector_t xlb;
		xlb.setConstant(10);
		state_vector_t xub;
		xub.setConstant(-99);

		my_dt.SetStateBounds(xlb, xub);

		std::cout << "This should be 1 : "
				<< my_dt.ValidateStatesActionsBounds() << std::endl;
	}

	{
		std::cout << "*** Setting good state and control bounds ***"
				<< std::endl;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);
		state_vector_t xlb;
		xlb.setConstant(-99);
		state_vector_t xub;
		xub.setConstant(100);

		control_vector_t ulb;
		ulb.setConstant(-9);
		control_vector_t uub;
		uub.setConstant(10);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		std::cout << "This should be 0 : "
				<< my_dt.ValidateStatesActionsBounds() << std::endl;
	}

	std::cout << ">>>>>>>>>> setSizeOfVariables()" << std::endl;
	{

		number_of_nodes = 6;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetSizeOfVariables();
		std::cout << std::endl;

	}

	std::cout << ">>>>>>>>>> setSizeOfConstraints()" << std::endl;
	{

		std::cout << "*** FREE time intervals  (6 nodes) ***" << std::endl;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetSizeOfVariables();

		my_dt.SetVerbosity(true);
		my_dt.SetSizeOfConstraints();
		std::cout << std::endl;

	}
	{

		std::cout << "*** CONSTANT time intervals (10 nodes) ***" << std::endl;

		number_of_nodes = 10;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetSizeOfVariables();

		my_dt.SetVerbosity(true);
		my_dt.SetEvenTimeIncrements(true);
		my_dt.SetSizeOfConstraints();
		std::cout << std::endl;

	}

	std::cout << ">>>>>>>>>> setupBounds()" << std::endl;
	{

		number_of_nodes = 6;
		duration[0] = 2;
		duration[1] = 6;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);

		state_vector_t xlb;
		xlb.setConstant(-99);
		state_vector_t xub;
		xub.setConstant(100);

		control_vector_t ulb;
		ulb.setConstant(-9);
		control_vector_t uub;
		uub.setConstant(10);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		my_dt.SetSizeOfVariables();
		my_dt.SetEvenTimeIncrements(true);
		my_dt.SetSizeOfConstraints();
		my_dt.setTimeIncrementsBounds(0.01, 0.05);

		my_dt.SetVerbosity(true);
		my_dt.SetBounds();
		std::cout << std::endl;

	}
	{

		std::cout << "*** FREE time intervals (6 nodes) ***" << std::endl;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);

		state_vector_t xlb;
		xlb.setConstant(-99);
		state_vector_t xub;
		xub.setConstant(100);

		control_vector_t ulb;
		ulb.setConstant(-9);
		control_vector_t uub;
		uub.setConstant(10);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		my_dt.SetSizeOfVariables();
		my_dt.SetEvenTimeIncrements(false);
		my_dt.SetSizeOfConstraints();
		my_dt.setTimeIncrementsBounds(0.01, 0.05);

		my_dt.SetVerbosity(true);
		my_dt.SetBounds();
		std::cout << std::endl;

	}

	{

		std::cout << "*** initial and final state / control specification ***"
				<< std::endl;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(true);
		my_dt.setTimeIncrementsBounds(0.01, 0.05);

		state_vector_t y_0;
		y_0.setConstant(-88);
		state_vector_t y_f;
		y_f.setConstant(44);

		state_vector_t xlb;
		xlb.setConstant(-99);
		state_vector_t xub;
		xub.setConstant(100);

		control_vector_t ulb;
		ulb.setConstant(-9);
		control_vector_t uub;
		uub.setConstant(10);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		my_dt.SetInitialState(y_0);
		my_dt.SetFinalState(y_f);

		my_dt.SetSizeOfVariables();
		my_dt.SetSizeOfConstraints();

		my_dt.SetVerbosity(true);
		my_dt.SetBounds();
		std::cout << std::endl;

	}

	state_vector_t y_0;
	state_vector_t y_f;
	y_0.setConstant(-88);
	y_f.setConstant(44);

	state_vector_t xlb;
	xlb.setConstant(-99);
	state_vector_t xub;
	xub.setConstant(100);

	control_vector_t ulb;
	ulb.setConstant(-9);
	control_vector_t uub;
	uub.setConstant(10);

	std::cout << ">>>>>>>>>> getNumNonzerosConstraintJacobian()" << std::endl;
	{

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(false);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		my_dt.SetInitialState(y_0);
		my_dt.SetFinalState(y_f);

		my_dt.SetSizeOfVariables();
		my_dt.SetSizeOfConstraints();
		my_dt.SetBounds();

		my_dt.SetVerbosity(true);

		int a = my_dt.getNumNonzerosConstraintJacobian();
		std::cout << "getNumNonzerosConstraintJacobian() : " << a << std::endl;

	}

	std::cout << ">>>>>>>>>> setSparsityJacobianConstraint()" << std::endl;
	{
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(true);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		my_dt.SetInitialState(y_0);
		my_dt.SetFinalState(y_f);

		my_dt.SetSizeOfVariables();
		my_dt.SetSizeOfConstraints();
		my_dt.SetBounds();

		my_dt.SetVerbosity(true);

//		my_dt.SetupNLProgram();

	}
	std::cout << ">>>>>>>>>> setSparsityJacobianCost()" << std::endl;
	{

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(false);

		my_dt.SetStateBounds(xlb, xub);
		my_dt.SetControlBounds(ulb, uub);

		my_dt.SetInitialState(y_0);
		my_dt.SetFinalState(y_f);

		my_dt.SetSizeOfVariables();
		my_dt.SetSizeOfConstraints();
		my_dt.SetBounds();

		my_dt.SetVerbosity(true);

//		my_dt.SetupNLProgram();
	}
// END OF UNIT TEST FOR SETUP

// UNIT TEST FOR SOLVER FUNCTIONS

	int num_states = acrobot::acrobotDimensions::STATE_SIZE;
	int num_controls = acrobot::acrobotDimensions::CONTROL_SIZE;
	int num_vars = 6 * (num_states + num_controls + 1) - 1;
	Eigen::VectorXd original_x = Eigen::VectorXd::LinSpaced(num_vars, 0.1,
			num_vars);
	Eigen::Map<Eigen::VectorXd> x_local(original_x.data(), num_vars);

	std::cout
			<< ">>>>>>>>> getStateControlIncrementsTrajectoriesFromDecisionVariables() "
			<< std::endl;
	{
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(false);

		my_dt.Initialize();

		state_vector_array_t x_trajectory;
		control_vector_array_t u_trajectory;
		Eigen::VectorXd h_trajectory;

		my_dt.getStateControlIncrementsTrajectoriesFromDecisionVariables(
				x_local, x_trajectory, u_trajectory, h_trajectory);

		std::cout << "[VERBOSE] desicion_variables : " << x_local.transpose()
				<< std::endl << std::endl;

		std::cout << "[VERBOSE] h_trajectory : " << h_trajectory.transpose()
				<< std::endl;

		for (int k = 0; k < 6; k++) {
			std::cout << "[VERBOSE] x_trajectory [" << k << "] : "
					<< x_trajectory[k].transpose() << std::endl;
			std::cout << "[VERBOSE] u_trajectory [" << k << "] : "
					<< u_trajectory[k].transpose() << std::endl;
		}
	}

	std::cout
			<< ">>>>>>>>> getStateControlIncrementsTrajectoriesFromDecisionVariables() "
			<< std::endl;
	{
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(false);

		my_dt.Initialize();

		double integrated_cost_funtion = my_dt.getObjective(original_x.data());

		std::cout << "[VERBOSE] integrated_cost_function : "
				<< integrated_cost_funtion << std::endl << std::endl;

	}
	std::cout << ">>>>>>>>> NLP Constraints " << std::endl;
	{
		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(false);

		my_dt.Initialize();

		//state_vector_array_t x_trajectory;
		//control_vector_array_t u_trajectory;
		//Eigen::VectorXd h_trajectory;

		//my_dt.getStateControlIncrementsTrajectoriesFromDecisionVariables(x_local,x_trajectory,u_trajectory,h_trajectory);
		//costFunction->setCurrentTrajectory(x_trajectory,u_trajectory,h_trajectory);
		//costFunction->gradient_xuh(val_vec);

		double *val = new double[35];

		Eigen::Map<Eigen::VectorXd> local_vec(val, 35);

		my_dt.evalSparseJacobianCost(val, original_x.data());

		std::cout << "JacobianCost: " << local_vec.transpose() << std::endl;

	}
	std::cout << ">>>>>>>>>>>> evaluateDeffects() " << std::endl;
	{
		test_interface_t my_dt(dynamics,
				derivatives, costFunction, noconstraints, duration,
				number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(true);

		my_dt.Initialize();

		Eigen::VectorXd defect_vector;

		my_dt.evaluateDefects(x_local, defect_vector);

		// TODO cannot set dircol method directly
		std::cout << "[VERBOSE] Defect Vector <trapezoidal>: "
				<< defect_vector.transpose() << std::endl;

		defect_vector.setZero();

		my_dt.evaluateDefects(x_local, defect_vector);

		std::cout << "[VERBOSE] Defect Vector <hermite>: "
				<< defect_vector.transpose() << std::endl;

		Eigen::VectorXd parametric_vector;
		my_dt.evaluateParametricConstraints(x_local, parametric_vector);

		std::cout << "[VERBOSE] : parametric_vector : "
				<< parametric_vector.transpose() << std::endl;

		Eigen::VectorXd constraints_vector;
		my_dt.evaluateUserConstraints(x_local, constraints_vector);

		std::cout << "[VERBOSE] : user constraints : "
				<< constraints_vector.transpose() << std::endl;

		int constraint_pointer_size = my_dt.getNumF();

		double* f_constraint_pointer = new double[constraint_pointer_size];

		my_dt.getConstraintsFct(f_constraint_pointer, original_x.data(),
				constraint_pointer_size);

		Eigen::Map<Eigen::VectorXd> local_constraints(f_constraint_pointer,
				constraint_pointer_size);

		std::cout << "[VERBOSE] : F_vector : " << local_constraints.transpose()
				<< std::endl;

		std::getchar();
	}

	std::cout << ">>>>>>> evalSparseJacobianConstraint() " << std::endl;
	{
		dircol_method = true;

		test_interface_t my_dt(dynamics, derivatives,
				costFunction, noconstraints, duration, number_of_nodes, methodVerbose);

		my_dt.SetVerbosity(false);
		my_dt.SetEvenTimeIncrements(true);
		my_dt.Initialize();

		int size_of_JacobianConstraint = my_dt.getDimG();

		std::cout << "lenG : " << size_of_JacobianConstraint << std::endl;

		double * val = new double[size_of_JacobianConstraint];

		my_dt.SetVerbosity(true);

		my_dt.evalSparseJacobianConstraint(val, original_x.data());

		Eigen::Map<Eigen::VectorXd> local_Jacobian(val,
				size_of_JacobianConstraint);

		std::cout << "[VERBOSE] : local_Jacobian : "
				<< local_Jacobian.transpose() << std::endl;

	}

	return 0;
}

#endif
