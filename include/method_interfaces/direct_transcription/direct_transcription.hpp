/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*!  \file 		direct_transcription.hpp
 * 	 \brief 	Direct transcription class for trajectory optimization
 * 	 \author 	depardo
 */

#ifndef DIRECTRANSCRIPTION_HPP_
#define DIRECTRANSCRIPTION_HPP_

#include <stdexcept>
#include <vector>
#include <iostream>
#include <fstream>

#include <memory>
#include <Eigen/Dense>

#include <dynamical_systems/base/DynamicsBase.hpp>
#include <dynamical_systems/base/DerivativesBaseDS.hpp>
#include <optimization/costs/CostFunctionBase.hpp>
#include <optimization/constraints/ConstraintsBase.hpp>
#include <method_interfaces/base_method_interface.hpp>

namespace DirectTrajectoryOptimization {

/**
 * @brief This class implements Direct transcription.
 * @details Implementing interfaces for DTO methods
 */
template<class DIMENSIONS>
class DTODirectTranscription: public DTOMethodInterface<DIMENSIONS> {

public:
	//Required as class has Eigen Members
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
	typedef typename DIMENSIONS::control_gain_matrix_t control_gain_matrix_t;
	typedef typename DIMENSIONS::state_matrix_t state_matrix_t;

	/**
	 * @brief Constructor of DTODirectTranscription
	 * @param[in] dynamics The vector of dynamics
	 * @param[in] derivatives The vector of derivatives
	 * @param[in] costFunction The cost function
	 * @param[in] constraints The vector of constraints
	 * @param[in] duration
	 * @param[in] number_of_nodes The number of nodes for discretization
	 * @param[in] methodVerbose The flag for method verbosity
	 */
	DTODirectTranscription(
			std::vector<std::shared_ptr<DynamicsBase<DIMENSIONS> > > dynamics,
			std::vector<std::shared_ptr<DerivativesBaseDS<DIMENSIONS> > > derivatives,
			std::shared_ptr<BaseClass::CostFunctionBase<DIMENSIONS> > costFunction,
			std::vector<std::shared_ptr<BaseClass::ConstraintsBase<DIMENSIONS> > > constraints,
			Eigen::Vector2d duration, int number_of_nodes, bool methodVerbose) :
			DTOMethodInterface<DIMENSIONS>(dynamics, derivatives, costFunction,
					constraints, duration, number_of_nodes, methodVerbose) {

		if (methodVerbose) {
			std::cout << "DTO Object successfully created" << std::endl;
		}
	}

	virtual ~DTODirectTranscription() {}

//protected: ToDo solve once test friends implemented

	virtual void evalSparseJacobianConstraint(double* val) override;
	virtual void evaluateDefects(Eigen::VectorXd & defect_vector) override;

	virtual void SetDircolMethod(const int &dcm) override;

	int dircol_method = 0;
    state_matrix_t I_y = state_matrix_t::Identity();
};

} // namespace DirectTrajectoryOptimization

#include <method_interfaces/direct_transcription/direct_transcription_implementation.hpp>

#endif /* DIRECTRANSCRIPTION_HPP_ */
