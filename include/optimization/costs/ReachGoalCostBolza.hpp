/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file ReachGoalCostBolza.hpp
 *	\brief Bolza type cost function minimizing the distance to a goal state
 *  \author depardo
 */

#ifndef REACHGOALCOSTBOLZA_HPP_
#define REACHGOALCOSTBOLZA_HPP_

#include <optimization/costs/CostFunctionBolza.hpp>

namespace DirectTrajectoryOptimization {
namespace Costs {

/**
 * \brief Bolza type cost function minimizing the distance to a goal state
 */
template<class DIMENSIONS>
class ReachGoalCostBolza : public BaseClass::CostFunctionBolza<DIMENSIONS> {

public :

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::state_matrix_t state_matrix_t;

	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::control_matrix_t control_matrix_t;

	/**
	 *  \brief Constructor
	 *  \param[in] goal The goal state
	 *  \param[in] Q	The weighting matrix for the intermediate cost (distance to goal)
	 *  \param[in] H	The weighting matrix for the final cost (distance to goal)
	 */
	ReachGoalCostBolza(const state_vector_t & goal, const state_matrix_t & Q=state_matrix_t::Identity(), const state_matrix_t & H = state_matrix_t::Identity()):Q_(Q),goal_(goal),H_(H)
	{}

	double phi(const state_vector_t & x, const control_vector_t & u);
	state_vector_t phi_x(const state_vector_t &x, const control_vector_t & u);
	control_vector_t phi_u(const state_vector_t &x, const control_vector_t & u);

	double terminalCost(const state_vector_t &x_f);
	state_vector_t terminalCostDerivative(const state_vector_t &x_f);

	virtual ~ReachGoalCostBolza() {}

	state_vector_t goal_;
	state_matrix_t Q_;
	state_matrix_t H_;
};

template <class DIMENSIONS>
double ReachGoalCostBolza<DIMENSIONS>::phi(const state_vector_t & x, const control_vector_t & u) {

	double val = (x-goal_).transpose()*Q_*(x-goal_);
	return val;
}

template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t ReachGoalCostBolza<DIMENSIONS>::phi_x(const state_vector_t & x,
		const control_vector_t & u ) {

	return(2*Q_*(x-goal_));
}

template <class DIMENSIONS>
typename DIMENSIONS::control_vector_t ReachGoalCostBolza<DIMENSIONS>::phi_u(const state_vector_t & x,
		const control_vector_t & u) {

	return(control_vector_t::Zero());
}


template <class DIMENSIONS>
double ReachGoalCostBolza<DIMENSIONS>::terminalCost(const state_vector_t & x_f) {

	double terminal_cost = (x_f-goal_).transpose()*H_*(x_f-goal_);

	return terminal_cost;
}

template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t ReachGoalCostBolza<DIMENSIONS>::terminalCostDerivative(const state_vector_t & x_f) {

	return(2*H_*(x_f-goal_));

}
}
}
#endif /* REACHGOALCOSTBOLZA_HPP_ */
