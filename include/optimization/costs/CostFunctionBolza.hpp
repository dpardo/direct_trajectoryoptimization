/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file CostFunctionBolza.hpp
 *	\brief Base class to derive cost function of the type J = sum_k( phi_k(x_k,u_k,k) ) +  H(X_K)
 *  \author depardo
 */

#ifndef COSTFUNCTIONBOLZA_HPP_
#define COSTFUNCTIONBOLZA_HPP_

#include <Eigen/Dense>
#include <optimization/costs/CostFunctionBase.hpp>

namespace DirectTrajectoryOptimization {
namespace BaseClass {

/**
 * \brief Base class to derive cost function of the type J = sum_k( phi_k(x_k,u_k,k) ) +  H(X_K)
 */
template <class DIMENSIONS>
class CostFunctionBolza : public CostFunctionBase<DIMENSIONS> {
public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;
	typedef typename DIMENSIONS::state_matrix_t state_matrix_t;

	CostFunctionBolza()	{

	}

	virtual ~CostFunctionBolza() {}

	int getNumberOfDependentVariables(){

		// This applies for all the cost functions derived from CostFunction Bolza
		int size_of_jacobian = this->state_size + this->control_size + 1;
		return(size_of_jacobian);
	}

	/**
	 * \brief Intermediate cost function
	 * \param[in] x sate vector
	 * \param[in] u control vector
	 * \param[in] h increment
	 * \return value of the intermediate/instantaneous cost function
	 */
	double IntermediateCost(const state_vector_t &x , const control_vector_t & u , const double & h);
	void gradient_xuh(const state_vector_t &x , const control_vector_t & u , const double & h , Eigen::VectorXd & f0_gradient);
	double evaluate();
	Eigen::VectorXd evaluate_gradient();

private :

	// phi can only depend on x and u
	virtual double phi(const state_vector_t & x, const control_vector_t & u) = 0;
	virtual state_vector_t phi_x(const state_vector_t &x, const control_vector_t & u) = 0;
	virtual control_vector_t phi_u(const state_vector_t &x, const control_vector_t & u) = 0;
	// There is no implementation of phi_h as it is always phi(x,u)
	virtual double terminalCost(const state_vector_t &x_f) = 0;
	virtual state_vector_t terminalCostDerivative(const state_vector_t & x) = 0;
};

template <class DIMENSIONS>
double CostFunctionBolza<DIMENSIONS>::evaluate() {

	double total_trajectory_cost = 0;
	int inc=0;
	for (int i = 0 ; i < this->trajectory_size-1 ; i++ ) {
		if(!(this->left_index.array()==i).any()) {
			total_trajectory_cost+=IntermediateCost(this->x_trajectory[i],this->u_trajectory[i],this->h_trajectory(inc));
			inc++;
		}
	}

	total_trajectory_cost = total_trajectory_cost/inc;

	total_trajectory_cost += this->terminalCost(this->x_trajectory.back());

	return(total_trajectory_cost);
}

template <class DIMENSIONS>
Eigen::VectorXd CostFunctionBolza<DIMENSIONS>::evaluate_gradient() {

	this->cost_function_jacobian_size = (this->trajectory_size - 1 - this->left_index.size()) * (this->state_size + this->control_size) + this->h_trajectory.size() + this->state_size;
	this->node_jacobian_size = this->state_size + this->control_size + 1;

	Eigen::VectorXd local(this->node_jacobian_size);
	Eigen::VectorXd jacobian_vector(this->cost_function_jacobian_size);

	int j_index = 0;
	int inc=0;
	for (int k = 0 ; k < this->trajectory_size-1 ; k++ ) {

		if (!(this->left_index.array()==k).any()) {

			gradient_xuh(this->x_trajectory[k],this->u_trajectory[k],this->h_trajectory(inc),local);
			jacobian_vector.segment(j_index,this->node_jacobian_size)=local;
			j_index += this->node_jacobian_size;
			inc++;
		}
	}
	jacobian_vector = jacobian_vector/inc;

	jacobian_vector.segment(j_index,this->state_size) = this->terminalCostDerivative(this->x_trajectory.back());
	//jacobian_vector.segment(j_index+this->state_size,this->control_size) = control_vector_t::Zero();

	return(jacobian_vector);
}

template <class DIMENSIONS>
double CostFunctionBolza<DIMENSIONS>::IntermediateCost(const state_vector_t &x , const control_vector_t & u ,
		const double & h) {

		//Intermediate cost is NOT necessarily the same as g(x,u)
		double increment_cost = phi(x,u);
	    return(increment_cost);
}

template <class DIMENSIONS>
void CostFunctionBolza<DIMENSIONS>::gradient_xuh(const state_vector_t &x , const control_vector_t & u ,
		const double & h , Eigen::VectorXd & f0_gradient) {

	//f0 already has the right size
	f0_gradient(0) = 0;
	f0_gradient.segment(1,this->state_size) = phi_x(x,u);
	f0_gradient.segment(1+this->state_size,this->control_size) = phi_u(x,u);
}

} //BaseClass
} //DirectTrajectoryOptimization

#endif /*COSTFUNCTIONBOLZA_HPP_*/
