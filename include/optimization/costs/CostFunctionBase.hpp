/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file CostFunctionBase.hpp
 *	\brief Base class to derive cost functions
 *  \authors neunertm,depardo
 */

#ifndef COSTFUNCTIONBASE_HPP_
#define COSTFUNCTIONBASE_HPP_

#include <Eigen/Dense>

namespace DirectTrajectoryOptimization {
namespace BaseClass{

/*
 *  \brief Base class to derive cost functions
 */
template <class DIMENSIONS>
class CostFunctionBase {
public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::state_vector_array_t state_vector_array_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	typedef typename DIMENSIONS::control_vector_array_t control_vector_array_t;

	CostFunctionBase() {

		state_size = DIMENSIONS::DimensionsSize::STATE_SIZE;
		control_size = DIMENSIONS::DimensionsSize::CONTROL_SIZE;

	};
	virtual ~CostFunctionBase() {};

	virtual double IntermediateCost(const state_vector_t & x_k , const control_vector_t & u_k , const double & h_k) = 0;

	virtual int getNumberOfDependentVariables() = 0 ;

	virtual void gradient_xuh(const state_vector_t & x_k , const control_vector_t & u_k , const double & h_k, Eigen::VectorXd & f0_gradient) = 0;

	virtual double evaluate() = 0;
	virtual Eigen::VectorXd evaluate_gradient() = 0;

	virtual void setCurrentTrajectory(const state_vector_array_t & x_t , const control_vector_array_t & u_t , const Eigen::VectorXd & h,
			const Eigen::VectorXi & l_index = Eigen::VectorXi()) {

		x_trajectory = x_t;
		u_trajectory = u_t;
		h_trajectory = h;
		left_index = l_index;
		trajectory_size = x_trajectory.size();
	}

protected:

	int state_size = 0;
	int control_size = 0;

	state_vector_array_t x_trajectory;
	control_vector_array_t u_trajectory;
	Eigen::VectorXd h_trajectory;
	Eigen::VectorXi left_index;
	int trajectory_size = 0;

	int cost_function_jacobian_size = 0;
	int node_jacobian_size = 0;
};
} // namespace BaseClass
} // namespace DirectTrajectoryOptimization

#endif /* COSTFUNCTIONBASE_HPP_ */
