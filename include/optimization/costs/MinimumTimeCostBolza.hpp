/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 	MinimumTimeCostBolza.hpp
 * \brief 	Bolza type cost function minimizing the total trajectory time
 * \author  depardo
 */

#ifndef MINIMUMTIMECOSTBOLZA_HPP_
#define MINIMUMTIMECOSTBOLZA_HPP_

#include <optimization/costs/CostFunctionBolza.hpp>

namespace DirectTrajectoryOptimization {
namespace Costs {
/**
 *  \brief Bolza type cost function minimizing the total trajectory time
 */
template<class DIMENSIONS>
class MinimumTimeCostBolza : public BaseClass::CostFunctionBolza<DIMENSIONS> {

public :

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	MinimumTimeCostBolza(){}

	virtual ~MinimumTimeCostBolza() {}

private :

	double phi(const state_vector_t & x, const control_vector_t & u , const double & h);
	state_vector_t phi_x(const state_vector_t &x, const control_vector_t & u , const double &h);
	control_vector_t phi_u(const state_vector_t &x, const control_vector_t & u , const double &h);
	double phi_h(const state_vector_t &x, const control_vector_t & u , const double &h);

	double terminalCost(const state_vector_t &x_f);
	state_vector_t terminalCostDerivative(const state_vector_t &x_f);

};

template <class DIMENSIONS>
double MinimumTimeCostBolza<DIMENSIONS>::phi(const state_vector_t & x, const control_vector_t & u , const double & h){

	return h;
}
template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t MinimumTimeCostBolza<DIMENSIONS>::phi_x(const state_vector_t &x, const control_vector_t & u , const double &h){

	return(state_vector_t::Zero());
}

template <class DIMENSIONS>
typename DIMENSIONS::control_vector_t MinimumTimeCostBolza<DIMENSIONS>::phi_u(const state_vector_t &x, const control_vector_t & u , const double &h){

	return(state_vector_t::Zero());
}

template <class DIMENSIONS>
double MinimumTimeCostBolza<DIMENSIONS>::phi_h(const state_vector_t & x , const control_vector_t &u , const double & h){

	return(1);
}

template <class DIMENSIONS>
double MinimumTimeCostBolza<DIMENSIONS>::terminalCost(const state_vector_t &x_f){

	return(0);
}

template <class DIMENSIONS>
typename DIMENSIONS::state_vector_t  MinimumTimeCostBolza<DIMENSIONS>::terminalCostDerivative(const state_vector_t &x_f){

	return(state_vector_t::Zero());
}
}
}
#endif /* MINIMUMTIMECOSTBOLZA_HPP_ */
