/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * GuardBase.hpp
 *
 *  Created on: Sep 5, 2016
 *      Author: depardo
 */

#ifndef _GuardBase_HPP_
#define _GuardBase_HPP_

#include <Eigen/Dense>
#include <memory>
#include <optimization/constraints/GenericConstraintsBase.hpp>

namespace DirectTrajectoryOptimization{
namespace BaseClass{

template <class DIMENSIONS>
class GuardBase : public NumDiffDerivativesBase,
					public std::enable_shared_from_this<GuardBase<DIMENSIONS> > {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;
	static const int s_size = static_cast<int>(DIMENSIONS::DimensionsSize::STATE_SIZE);

	/**
	 * Guards
	 */
	GuardBase(const bool & no_guard) {
	  SetSize(1);
	  SetBounds(Eigen::VectorXd::Constant(1,-1e20),Eigen::VectorXd::Constant(1,1e20));
	}
	GuardBase(){}

	virtual ~GuardBase(){ };

  int GetSize() {return complete_vector_size;}
  Eigen::VectorXd GetLowerBound() {return guard_lb; }
  Eigen::VectorXd GetUpperBound() {return guard_ub; }
	void SetSize(const int & sog);
	void SetBounds(const Eigen::VectorXd & lb, const Eigen::VectorXd & ub);
	void initialize();
	void initialize_num_diff();
	void fx(const Eigen::VectorXd & in, Eigen::VectorXd & out);

	virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x1);
	Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t & x1);

  std::shared_ptr<GuardBase<DIMENSIONS> > my_pointer;

  int complete_vector_size = 0;
  int size_of_jacobian = 0;
  int total_inputs_jacobian=0;

private:

  Eigen::VectorXd guard_lb;
  Eigen::VectorXd guard_ub;

	bool sog_ready_ = false;
	bool bounds_ready_ = false;

};

template <class DIMENSIONS>
void GuardBase<DIMENSIONS>::SetSize(const int & sog) {
  if(sog < 1) {
      std::cout << "Guard size must be greater than 0" << std::endl;
      std::exit(EXIT_FAILURE);
  }
  complete_vector_size = sog;
  sog_ready_ = true;
}

template <class DIMENSIONS>
void GuardBase<DIMENSIONS>::SetBounds(const Eigen::VectorXd & lb, const Eigen::VectorXd & ub) {
  if(!(lb.size() == complete_vector_size) || !(ub.size() == complete_vector_size)) {
      std::cout << "Guard bounds size must be equal to SoG" << std::endl;
      std::exit(EXIT_FAILURE);
  }
  guard_lb = lb;
  guard_ub = ub;
  bounds_ready_ = true;
}

template <class DIMENSIONS>
void GuardBase<DIMENSIONS>::initialize() {

  if(!sog_ready_ || !bounds_ready_) {
      std::cout << "guard is not ready!" << std::endl;
      std::exit(EXIT_FAILURE);
  }

	// This cannot change
	total_inputs_jacobian = s_size;

	// This cannot change
	size_of_jacobian = complete_vector_size*total_inputs_jacobian;

	initialize_num_diff();
}

template <class DIMENSIONS>
Eigen::VectorXd GuardBase<DIMENSIONS>::evalConstraintsFct(const state_vector_t& x1) {

	// always possible!
	return (Eigen::VectorXd::Constant(1,0.0));
}

template <class DIMENSIONS>
Eigen::VectorXd GuardBase<DIMENSIONS>::evalConstraintsFctDerivatives(const state_vector_t & x1) {

	this->numDiff->df(x1,this->mJacobian);

	// The Jacobian of the guards is defined as row_major: repeating the constraints!
	//returning ROW-wise the constraint as a vector
  Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor> temp(this->mJacobian);
	return (Eigen::Map<Eigen::VectorXd>(temp.data(),size_of_jacobian));
}

template <class DIMENSIONS>
void GuardBase<DIMENSIONS>::fx(const Eigen::VectorXd & inputs, Eigen::VectorXd & outputs) {
	outputs = evalConstraintsFct(inputs);
}

template <class DIMENSIONS>
void GuardBase<DIMENSIONS>::initialize_num_diff() {

	this->my_pointer = std::enable_shared_from_this<GuardBase<DIMENSIONS>>::shared_from_this();
	this->mJacobian.resize(complete_vector_size,total_inputs_jacobian);
	this->numdifoperator = std::shared_ptr<FunctionOperator>(new FunctionOperator(total_inputs_jacobian, complete_vector_size , this->my_pointer));
	this->numDiff = std::shared_ptr<Eigen::NumericalDiff<FunctionOperator> >( new Eigen::NumericalDiff<FunctionOperator>(*this->numdifoperator,std::numeric_limits<double>::epsilon()));
}

template <class DIMENSIONS>
const int GuardBase<DIMENSIONS>::s_size;

} // BaseClass
} // DirectTrajectoryOptimization
#endif /* _GuardBase_HPP_ */
