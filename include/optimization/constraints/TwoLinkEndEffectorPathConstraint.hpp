/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*
 * TwoLinkEndEffectorPathConstraint.hpp
 *
 *  Created on: Jan 7, 2016
 *      Author: hsingh
 */

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#ifndef TWOLINKENDEFFECTORPATHCONSTRAINT_HPP_
#define TWOLINKENDEFFECTORPATHCONSTRAINT_HPP_

#include <optimization/constraints/ConstraintsBase.hpp>

namespace DirectTrajectoryOptimization {

template<class DIMENSIONS>
class TwoLinkEndEffectorPathConstraint: public BaseClass::ConstraintsBase<DIMENSIONS> {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	typedef typename DIMENSIONS::state_vector_t state_vector_t;
	typedef typename DIMENSIONS::control_vector_t control_vector_t;

	TwoLinkEndEffectorPathConstraint(double lenA, double lenB) :
		_lenA(lenA),
		_lenB(lenB) {
		initialize();
	}

	void initialize();

	Eigen::VectorXd evalConstraintsFct(const state_vector_t& x,
			const control_vector_t& u);

	Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t & x,
			const control_vector_t & u);

	~TwoLinkEndEffectorPathConstraint() {}

	double _lenA, _lenB;		// link lengths

};

template<class DIMENSIONS>
void TwoLinkEndEffectorPathConstraint<DIMENSIONS>::initialize() {

	this->g_size = 1;
	this->g_low.resize(this->g_size);
	this->g_up.resize(this->g_size);

	// set upper and lower bounds (improve this)
	this->g_low[0] = 0;
	this->g_up[0] = 0;

	int number_of_dependent_variables_at_each_constraint = 2;

	this->nnz_jac_ = number_of_dependent_variables_at_each_constraint
			* this->g_size;

	this->iRow_.resize(this->nnz_jac_);
	this->jCol_.resize(this->nnz_jac_);

	Eigen::VectorXi temp;
	temp = Eigen::VectorXi::LinSpaced(this->g_size, 0, this->g_size - 1);
	this->iRow_ = temp.replicate(
			number_of_dependent_variables_at_each_constraint, 1);

	temp = Eigen::VectorXi::LinSpaced(
			number_of_dependent_variables_at_each_constraint, 0,
			number_of_dependent_variables_at_each_constraint - 1);
	this->jCol_ = temp.replicate(this->g_size, 1);

}

template<class DIMENSIONS>
Eigen::VectorXd TwoLinkEndEffectorPathConstraint<DIMENSIONS>::evalConstraintsFct(
		const state_vector_t& x, const control_vector_t& u) {

	Eigen::VectorXd g_vector(this->g_size);

	// forward kinematics to find location of end effector
	double xPos = _lenA * sin(x[0]) + _lenB * (sin(x[0] + x[1]));
	double yPos = _lenA * cos(x[0]) + _lenB * (cos(x[0] + x[1]));

	double radius = sqrt(xPos*xPos + yPos*yPos);
	double angle = -1*atan2(yPos, xPos);

	// stay outside 120-150 deg cone
	if(angle > M_PI/6 && angle < M_PI/3) {
		g_vector[0] = 10*std::min(angle - M_PI/6, M_PI/3 - angle);
	} else {
		g_vector[0] = 0;
	}

	return g_vector;

}

template<class DIMENSIONS>
Eigen::VectorXd TwoLinkEndEffectorPathConstraint<DIMENSIONS>::evalConstraintsFctDerivatives(
		const state_vector_t & x, const control_vector_t &u) {

	// ToDo ...
	Eigen::VectorXd Jacobian(this->nnz_jac_);

	// first constraint
	// first w.r.t states
	Jacobian[0] = 2 * x(0);
	Jacobian[1] = 2 * x(1);
	Jacobian[2] = 0;
	Jacobian[3] = 0;

	// now w.r.t. control
	Jacobian[4] = 0;

// second constraint
	Jacobian[5] = 0;
	Jacobian[6] = 0;
	Jacobian[7] = 1;
	Jacobian[8] = 1;

	Jacobian[9] = 1;

// third constraint
	Jacobian[10] = -1;
	Jacobian[11] = -1;
	Jacobian[12] = -1;
	Jacobian[13] = -1;

	Jacobian[14] = -1;

	return Jacobian;

}
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
#endif /* TWOLINKENDEFFECTORPATHCONSTRAINT_HPP_ */

