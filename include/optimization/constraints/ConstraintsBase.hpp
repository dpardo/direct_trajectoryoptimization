/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/

/*! \file 	ConstraintsBase.hpp
 *	\brief 	Base class for constraints
 *	\author	lmoller , depardo
 */

#ifndef _CONSTRAINTSBASE_H
#define _CONSTRAINTSBASE_H

#include <Eigen/Dense>

namespace DirectTrajectoryOptimization {
/**
 * \brief Collection of base classes to interface with DirectTrajectoryOptimization
 */
namespace BaseClass{

/**
 * \brief	Base class for TO constraints
 */
template <class DIMENSIONS>
class ConstraintsBase {

public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    typedef typename DIMENSIONS::state_vector_t state_vector_t;
    typedef typename DIMENSIONS::control_vector_t control_vector_t;
    
    ConstraintsBase(){};
    virtual ~ConstraintsBase() {};
    
    virtual Eigen::VectorXd evalConstraintsFct(const state_vector_t& x, const control_vector_t& u) {
      return (Eigen::VectorXd::Zero(g_size));
    }
    virtual Eigen::VectorXd evalConstraintsFctDerivatives(const state_vector_t& x, const control_vector_t& u) {
      return (Eigen::VectorXd::Zero(nnz_jac_));
    }

    Eigen::VectorXd getConstraintsLowerBound() {return g_low;}
    Eigen::VectorXd getConstraintsUpperBound() {return g_up;}

    void setSize(const int & size) {g_size  = size;}
    void setConstraintsLowerBound(const Eigen::VectorXd & g_lb_candidate);
    void setConstraintsUpperBound(const Eigen::VectorXd & g_ub_candidate);

    int getNumConstraints() {return g_size;}
    
    /**
     * @brief Access the number of non-zero elements of the Jacobian of this constraint
     * @return The number of non-zero elements of the Jacobian
     */
    int getNnzJac() {return nnz_jac_;}

    Eigen::VectorXi getSparseRow() {return iRow_;}
    Eigen::VectorXi getSparseCol() {return jCol_;}

private:
    int g_size = 0;   /* constraint size */
    Eigen::VectorXd g_low;
    Eigen::VectorXd g_up;

protected:
    int nnz_jac_ = 0; /* jacobian size */
    Eigen::VectorXi iRow_ , jCol_; /* sparsity indexes */
};

template <class DIMENSIONS>
void ConstraintsBase<DIMENSIONS>::setConstraintsLowerBound(const Eigen::VectorXd & g_lb_candidate) {

  int candidate_size = g_lb_candidate.size();

  if (candidate_size != g_size) {
    std::cout << "ERROR : Lower bound vector size " << std::endl;
    std::exit(EXIT_FAILURE);
  } else {
    g_low = g_lb_candidate;
  }
}

template <class DIMENSIONS>
void ConstraintsBase<DIMENSIONS>::setConstraintsUpperBound(const Eigen::VectorXd & g_ub_candidate) {

  int candidate_size = g_ub_candidate.size();

  if (candidate_size != g_size) {
    std::cout << "ERROR : Lower bound vector size " << std::endl;
    std::exit(EXIT_FAILURE);
  } else {
    g_up = g_ub_candidate;
  }
}
}
}
#endif /* _CONSTRAINTSBASE_H */
